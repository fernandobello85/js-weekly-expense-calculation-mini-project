const presupuestoUsuario = prompt('Cual es el presupuesto semanal ?');
const formulario = document.getElementById('agregar-gasto');
let cantidadPresupuesto;

class Presupuesto{
    constructor(presupuesto) {
        this.presupuesto = Number(presupuesto);
        this.restante = Number(presupuesto);
    }
    presupuestoRestante(cantidad = 0) {
        return this.restante -= Number(cantidad);
    }
}

class Interface {
    insertarPresupuesto(cantidad) {
        const presupuestoSpan = document.querySelector('span#total');
        const restanteSpan = document.querySelector('span#restante');

        presupuestoSpan.innerHTML = `${cantidad}`
        restanteSpan.innerHTML = `${cantidad}`
    }
    imprimirMensaje(mensaje, tipo) {
        const divMensaje = document.createElement('div');
        divMensaje.classList.add('text-center', 'alert');

        const sendBtn = document.querySelector('.btn-primary');
        if(tipo === 'error') {
            divMensaje.classList.add('alert-danger');
        } else {
            divMensaje.classList.add('alert-success');
        }
        divMensaje.appendChild(document.createTextNode(mensaje))
        document.querySelector('.primario').insertBefore(divMensaje, formulario);
        sendBtn.disabled = true;

        setTimeout(function(){
            divMensaje.remove();
            formulario.reset();
            sendBtn.disabled = false;
        }, 2000);
    }
    agregarGastoListado(nombreGasto, cantidadGasto) {
        const gastoListado = document.querySelector('#gastos ul');
        const li = document.createElement('li');
        li.className = 'list-group-item d-flex justify-content-between align-items-center';
        li.innerHTML = `
            ${nombreGasto}
            <span class="badge badge-primary badge-pill">${cantidadGasto}</span>
        `;
        gastoListado.appendChild(li);

    }
    presupuestoRestante(cantidadGasto) {
        const restante = document.querySelector('span#restante');
        const presupuestoRestanteUsuario = cantidadPresupuesto.presupuestoRestante(cantidadGasto);
        restante.innerHTML = `
            ${presupuestoRestanteUsuario}`
        this.comprobarPresupuesto();
    }

    comprobarPresupuesto() {
        if (cantidadPresupuesto.presupuestoRestante() < presupuestoUsuario * 0.25) {
            console.log('menos del 25')
            document.querySelector('.restante').classList.remove('alert-success', 'alert-warning');
            document.querySelector('.restante').classList.add('alert-danger');
        } else if(cantidadPresupuesto.presupuestoRestante() < presupuestoUsuario * 0.5) {
            console.log('menos del 50')
            document.querySelector('.restante').classList.remove('alert-success');
            document.querySelector('.restante').classList.add('alert-warning');
        }

    }
}

document.addEventListener('DOMContentLoaded', function() {
    if(presupuestoUsuario === null || presupuestoUsuario === '' || isNaN(presupuestoUsuario)){
        window.location.reload();
    } else {
        cantidadPresupuesto = new Presupuesto(presupuestoUsuario);
        const ui = new Interface();
        ui.insertarPresupuesto(cantidadPresupuesto.presupuesto);
    }
})

formulario.addEventListener('submit', function(e) {
    e.preventDefault();
    const nombreGasto = document.getElementById('gasto').value;
    const cantidadGasto = document.getElementById('cantidad').value;

    const ui = new Interface();

    if(nombreGasto === '' || cantidadGasto === '') {
        ui.imprimirMensaje('Los campos deben ser diligenciados', 'error')
    } else {
        ui.imprimirMensaje('Correcto', 'correcto');
        ui.agregarGastoListado(nombreGasto, cantidadGasto);
        ui.presupuestoRestante(cantidadGasto)
    }
})